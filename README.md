[![DOI](https://joss.theoj.org/papers/10.21105/joss.04114/status.svg)](https://doi.org/10.21105/joss.04114)

# Description

This software estimates tagging latency for P300-based Brain-Computer Interfaces (BCI). It is designed for both regular screen and VR. Tagging marks the moment at which the user has been presented with a stimulus that generates a P300 response from the brain. Tagging latency is the time between the actual presentation of the stimulus on the screen and its position in the EEG signal. Tagging latency is important for correctly processing the P300 portion of an EEG signal.

Latency is corrected taking into account:
- The number of VR cameras within the physical screen
- The orientation of the screen
- The distribution of the items on screen
- The position of the photodiode used to measure hardware tagging latency of event-related potentials (ERPs) in the ongoing electroencephalogram (EEG). Learn more about this method [here](doc/Engineering%20Study%20HMD%20-%20BCI.pdf).
- The number of targets used to measure the latency using the photodiode, and:
    - The position of the target (if only one were used)
    - The distribution of these targets (if multiple were used)

A short introduction to this software in the form of a [paper](https://gitlab.com/programgreg/tagginglatencyestimator/-/jobs/artifacts/master/browse?job=paper) is provided (the link contains the most recent version, the official version of the paper is [here](https://doi.org/10.21105/joss.04114)). You can also learn more in [this technical paper](doc/About%20tagging%20latency%20in%20visual%20ERP.pdf) and this [presentation](doc/nbt_berlin.pptx).

# Usage

The software is implemented using Unity, which is a multiplatform application engine. 
Therefore, the project can be downloaded and cooked for Linux, Windows or Mac.

## Download

The project can be downloaded:

1. As a zip file:
![How to download zip](doc/readme_fig1.png)

2. Using `git` with SSH (requires to [define a SSH key](https://docs.gitlab.com/ee/ssh/) first)

```
> git clone git@gitlab.com:programgreg/tagginglatencyestimator.git
```

3. Using `git` with HTTPS:

```
> git clone https://gitlab.com/programgreg/tagginglatencyestimator.git
```

If you are not familiar with git, we recommend following this official [tutorial](https://git-scm.com/docs/gittutorial).

## Run project

See the [dedicated section](doc/run_project.md).

## Python

A python implementation of the `STaggingComputer` is also provided, for those having no particular affinity for C# and Unity.

Install the python module from the `PythonProject` folder, by running the folowing in a prompt:

```
> python setup.py develop
```

Test that the module was correctly installed by writing the folowing in a python console:

```
> import tle
> tle.__version__
```

This should output the current version of the python module.

Start using the python module by reading the use cases inside the [examples](PythonProject/examples) folder.

# Dependences

- Unity 2023.2.3.f1: This version can be installed using the Unity Hub. More information on the official [webpage](https://unity.com/fr/download)
- [UCoreNet](https://gitlab.com/programgreg/UCoreNet)

# Community Guidelines

Collaborators are welcomed to the project, and should not hesitate to rise an [issue](https://gitlab.com/programgreg/tagginglatencyestimator/-/issues) or contribute to the software. 
We use a simplified Gitworkflow described below:
- Fork the repository and create a new branch to work on a new functionnality/bug fixing
- Open a [merge request](https://gitlab.com/programgreg/tagginglatencyestimator/-/merge_requests) to merge your branch against the master branch in this repository
- In the description of the merge request, indicate the initial issue you were working on (create this issue if necessary)
- Address the review comments and wait for merging approval :)

The technological slack is based on Unity (C#) and Python. 

# References

Cattan, G., Andreev, A., Maureille, B., and Congedo, M. (2018). Analysis of tagging latency when comparing event-related potentials. Available at: https://hal.archives-ouvertes.fr/hal-01947551. (also included in the [doc](doc/About%20tagging%20latency%20in%20visual%20ERP.pdf) folder.)

Cattan, G. (2019). De la réalisation d’une interface cerveau-ordinateur pour une réalité virtuelle accessible au grand public. Available at: https://tel.archives-ouvertes.fr/tel-02357203.

A. Andreev, G. Cattan, and M. Congedo (2019). Engineering study on the use of Head-Mounted display for Brain- Computer Interface. Available at: https://hal.archives-ouvertes.fr/hal-02166844. (also included in the [doc](doc/Engineering%20Study%20HMD%20-%20BCI.pdf) folder.)

**Cite this software as**:

Cattan, G. and Mendoza, C. (2023). Tagging Latency Estimator: A Standalone Software for Estimating Latency of Event-Related Potentials in P300-based Brain-Computer Interfaces. Journal of Open Source Software.
DOI: [10.21105/joss.04114](https://doi.org/10.21105/joss.04114).


# License

This code is distributed under Apache License 2.0 by the IHMTEK company.

