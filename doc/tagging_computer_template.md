# TaggingComputer

## Tutorial

{description}

Let's open Unity Editor and have a look a the game object `TaggingComputer`.
Inside the inspector, we will configure the script `STaggingComputer` to match our example:

![](tagging_computer_details.png)

In the above screenshot, you can see the folowing parameters:
- `Number Of Trial For Barycenter Computation` (10000):  Let's say you have 3 aligned stimuli and you displayed only 2 targets, then this couple of target can only be on of these combinations: {1-1, 1-2, 1-3, 2-2, 2-3, 3-3}. If your photodiode was located on the first item, and you pick the couple {3-3} as a barycenter, you will obtain a latency which is maximal between your photodiode and the barycenter. However, in practice, it is likely that your barycenter will be closer to item {2} resulting in a lower latency. This settings help to dimish variability in the estimation of the barycenter, by settings the number of "picking".
- `Screen Width In Pixels` (1000) : the same as in the problem;
- `Screen Height In Pixels`: this data is not important here, because the screen is reverted;
- `Ui`: this data is not important here, because the screen is reverted;
- `Uj`: 250, as detailed in the initial statement;
- `Item Row Coord Where Was Located The Photodiode`: 0 (as per the initial statement);
- `Item Col Coord Where Was Located The Photodiode`: 3 (as per the initial statement);
- `Number Of Displayed Targets`: 10 (as per the initial statement);
- `Item Number In A Col`: 1 (as per the initial statement);
- `Item Number In A Row`: 3 (as per the initial statement);
- `Latency Measured By Photodiode` = 40 (as per the initial statement).

Once the fields are fulfilled, click on the compute button.
Let's have a look at the outputs:

- `Barycenter I`: Row index of the estimated barycenter. Here, it will be exactly the same as the photodiode because we have a reverted screen.
- `Barycenter J`: Column index of the estimated barycenter. Here, his value is very close to the one of the photodiode (~= 1). 
- `X Latency`: This value is close to zero, which confirm our previous observation, that is, that the estimated barycenter is close to the position of the photodiode. For non reverted screen, you should look at `Y Latency`.
- `X Latency Std`: This value reflects the incertaincy on our latency estimation, based on how well is our estimation of the barycenter. For non reverted screen, you should look at `Y latency Std`.
- ...other settings are mostly intermediate computational results.

## Description of the object fields

{table}