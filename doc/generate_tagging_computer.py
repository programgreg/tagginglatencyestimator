import re

####################################################

parameters = ""
names = []
descriptions = []
types = []


def substr(token_a:str, token_b:str or None=None, string:str=None):
    if not string:
        string = parameters
    ret = string.split(token_a)[1]
    if token_b:
        ret = ret.split(token_b)[0]
    return ret


def extract_description(token_a:str, token_b:str or None=None):
    description = substr(token_a, token_b).strip().replace("\n        ", " ")\
            .replace("[1]_ ", "")\
            .replace(" [1]_", "")
    descriptions.append(description)


def extract_type(token:str):
    type = substr(token, "\n")[3:].strip()
    types.append(type)
    return type


def get_md_table():
    table = ""
    table += "|Field|Type|Description|\n"
    table += "|---|---|---|\n"
    n = len(names)
    for i in range(n):
        table += "|" + names[i] + "|" + types[i] + "|" + descriptions[i] + "|\n"
    return table


def get_example_description():
    with open('..\\PythonProject\\examples\\correct_photodiode_latency.py') as file:
        data = file.read().rstrip()
        r = substr("\"\"\"", "Notes", data)
        return r


def read(filename:str):
    with open(filename, 'r') as file:
        data = file.read().rstrip()
        return data


def dump(filename:str, content:str):
    with open(filename, 'w') as file:
        file.write(content)


def extract_definitions():
    data = read('..\\PythonProject\\tle\\tagging_computer.py')
    global parameters
    parameters = data.split("----------")[1].split("Attributes")[0]
    global names
    names = re.findall('    (.*) : ', parameters)

    for i in range(0, len(names) - 1):
        type = extract_type(names[i])
        extract_description(names[i] + " : " + type, names[i+1])
        
    type = extract_type(names[i + 1])
    extract_description(names[i + 1] + " : " + type)


####################################################


extract_definitions()

md_table = get_md_table()

example_description = get_example_description()

data = read('tagging_computer_template.md')
data = data.replace("{description}", example_description)
data = data.replace("{table}", md_table)

dump('tagging_computer.md', data)