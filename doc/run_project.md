The project contains a sample scene to assist the user in estimating and interpreting the tagging latency. 
It also contains a script named `STaggingComputer` which can be used directly from within the Engine in edit mode, without having to play or cook the project. However, it does not provide assistance or interpretation of the results.

1. To test `STaggingComputer` in edit mode, open the project and locate a GameObject called `TaggingComputer` in the
hierarchy:

![GameObject TaggingComputer](doc/readme_fig2.png)

In the right side of the screen, you should see an inspector. You can modify some of the fields and click on compute:

![Inspector TaggingComputer](doc/readme_fig3.png)

The result of the computation will be stored in the disabled fields at the bottom of the inspector:

![Inspector TaggingComputer](doc/readme_fig4.png)

See [TaggingComputer](doc/tagging_computer.md) for an example tutorial using `TaggingComputer` with a detailed explanation of the fields in the inspector.

2. To test the sample scene, open the project and click on `Play`:

![Play button](doc/readme_fig5.png)

The elements in the `Game` window will become interactive so as in an standalone application:

![Game window](doc/readme_fig6.png)

We provide for ease of use window and linux binary of the sample scene in the form of an archive : 
`tle_v[X].zip` or `tle_v[X]_linux.zip` (depening on your operating platform).

Once extracted, the scene can be started right away by running `TaggingLatencyEstimator.exe` (Window) or `TaggingLatencyEstimator.x86_64` (Linux).

See [Sampe Scene](doc/sample_scene.md) for a detailed explanation of the steps in the sample scene.