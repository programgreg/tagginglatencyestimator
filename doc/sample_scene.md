# Sample Scene

For this tutorial, we will use the same example that is described in the [STaggingComputer](tagging_computer.md) page. Let's see together how to get an estimate of the tagging latency by using the sample scene:

1) Open the application and click on the option `No Vr`. This means that there is only one camera which is rendered on your screen. When multiple cameras are rendered into a screen, there are not always displayed within the same frame. In this case, it is recommended to report configuration of the first camera rendering stimuli (last option). The `VR` option simplify the determination of the first rendered camera if you are using a VR configuration (e.g., screen split in two).


![](step-1.png)

2) As the screen is reverted in the example, you need to click on the option at your right. In general, screens refresh from top to bottom in about 16ms, thus introduce a latency between stimuli in a column. At the inverse, a latency can be observed between stimuli in a row when screen is reverted.

![](step-2.png)

3) Just report here the number of simuli in a row, as per the initial statement of the experiment:

![](step-3.png)

4) You can here adjust manually the margin of your row thanks to the slider:

![](step-4.png)

5) Report here the number of targets that were displayed during your P300 experiment. This information will help in estimating the barycenter of the targets.

![](step-5.png)

6) If the distribution of the targets on screen is uniform, we can average the position of the displayed targets to estimate a barycenter. As in general the position of the targets are random and vary for each user, the program draws `X` random targets to estimate the barycenter, where `X` is the number of targets reported in the previous step.
If the distirbution is not uniform, the barycenter will be by default the left-top corner (0, 0).

![](step-6.png)

7) If you used a photodiode to estimate the latency of a specific stimulus, you can click on the screen at the location of the photodiode. As we use a reverted screen, only the position in a row is relevant here. Note that the software automatically add a graphical uncertainty due to the imprecision of the mouse click on screen. If the position of the photodiode is not known, it is set by default to the center of the screen.

![](step-7.png)

8) If you used a photodiode to estimate the latency of a particular stimulus, you can enter the time measured by the photodiode (in ms) during this step. If you did not use a photodiode, the standard deviation will be computed using an hypothetical photodiode placed at the center of the screen. Then, only the standard deviation of the latency will be reported, without latency.

![](step-8.png)

9) In this screen are reported the results of the computation. The software computed a standard deviation of about 1ms (some of the digits can change because targets are drawn randomly for barycenter estimation). It means, that even if our photodiode measured a latency of about 40ms, stimuli appeared on screen between 38-41ms earlier than the onset of the tag.

![](step-9.png)

10) The last screen allows you to repeat all steps for a different configuration and compare the results. For example, you may want to compare the ERP produced under two different experimental configurations A and B. Let's say you measured two different latencies using a photodiode and reset the time origin by removing the respective latency for each configuration. If the two configurations report high standard deviations stdA and stdB, it is possible that you observe a latency of maximum stdA + stdB between the two conditions which can be explained by software/material setup alone. 


![](step-10.png)