﻿using UnityEngine;
using UnityEngine.UI;

public class SAdjustU : SModel
{
    private Slider m_oUiSlider;
    private Slider m_oUjSlider;
    private InputField m_oUiValue;
    private InputField m_oUjValue;

    void OnEnable()
    {
        m_oUiSlider = Component<Slider>("Slider_Ui");
        m_oUjSlider = Component<Slider>("Slider_Uj");
        m_oUiValue = Component<InputField>("Ui_value");
        m_oUjValue = Component<InputField>("Uj_value");

        GameObject imagePC = Child("Image", 0);
        GameObject imageVR = Child("Image", 1);

        if (SVariables.s_eConfiguration.isVR())
        {
            imagePC.SetActive(false);
            imageVR.SetActive(true);
        }
        else
        {
            imagePC.SetActive(true);
            imageVR.SetActive(false);
        }

        SVariables.s_fUi = SVariables.s_eOrientation.isReverted() ? 0 : 100f / (SVariables.s_iNumRow + 1);
        SVariables.s_fUj = SVariables.s_eOrientation.isNotReverted() ? 0 : 100f / (SVariables.s_iNumCol + 1);
        m_oUiValue.text = SVariables.s_fUi.ToString();
        m_oUjValue.text = SVariables.s_fUj.ToString();
        m_oUiSlider.value = SVariables.s_fUi;
        m_oUjSlider.value = SVariables.s_fUj;

        m_oUiSlider.interactable = !SVariables.s_eOrientation.isReverted();
        m_oUjSlider.interactable = !SVariables.s_eOrientation.isNotReverted();

        m_oUiValue.interactable = !SVariables.s_eOrientation.isReverted();
        m_oUjValue.interactable = !SVariables.s_eOrientation.isNotReverted();

        ActiveGrid(true);
        EnablePhotodiodeDrawing(false);
    }

    public void SetUiSlider()
    {
        m_oUiValue.text = m_oUiSlider.value.ToString();
        SetUi();
    }

    public void SetUjSlider()
    {
        m_oUjValue.text = m_oUjSlider.value.ToString();
        SetUj();
    }

    public void SetUi()
    {
        string val = m_oUiValue.text;
        if (val != "")
        {
            SVariables.s_fUi = int.Parse(val);
            m_oUiSlider.value = SVariables.s_fUi;
        }
    }

    public void SetUj()
    {
        string val = m_oUjValue.text;
        if (val != "")
        {
            SVariables.s_fUj = int.Parse(val);
            m_oUjSlider.value = SVariables.s_fUj;
        }
    }

}
