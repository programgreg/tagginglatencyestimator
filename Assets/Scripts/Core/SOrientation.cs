﻿using UnityEngine.UI;

public class SOrientation : SModel
{

    private Toggle m_oNonReverted;
    private Toggle m_oReverted;

    void OnEnable()
    {
        m_oNonReverted = Component<Toggle>("Toggle_NonReverted");
        m_oReverted = Component<Toggle>("Toggle_Reverted");
        m_oNonReverted.isOn = false;
        m_oReverted.isOn = false;
    }

    public void Validate()
    {
        if (m_oNonReverted.isOn || m_oReverted.isOn)
            GoToNextStep();
    }

    public void SetReverted()
    {
        m_oNonReverted.isOn = false;
        SVariables.s_eOrientation = SVariables.Orientation.REVERTED;
    }

    public void SetNotReverted()
    {
        m_oReverted.isOn = false;
        SVariables.s_eOrientation = SVariables.Orientation.NOT_REVERTED;
    }

    public void SetIDontKnow()
    {
        SVariables.s_eOrientation = SVariables.Orientation.UNKNOWN;
        GoToNextStep();
    }
}
