﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SModel : MonoBehaviour
{

    private static int s_iNextStep = 0;
    protected GameObject m_goNextStep;
    protected GameObject m_goPreviousStep;
    protected static readonly int INVALID = SVariables.INVALID;

    void Awake()
    {
        GameObject parent = transform.parent.gameObject;
        ++s_iNextStep;
        
        try
        {
            m_goNextStep = Child(parent, s_iNextStep);
            if(s_iNextStep >= 2)
                m_goPreviousStep = Child(parent, s_iNextStep - 2);
        }
        catch (System.Exception)
        {
            s_iNextStep = 0;
            m_goNextStep = Child(parent, 0);
        }
    }

    public void GoToPreviousStep()
    {
        m_goPreviousStep.SetActive(true);
        gameObject.SetActive(false);
    }

    public void GoToNextStep()
    {
        m_goNextStep.SetActive(true);
        gameObject.SetActive(false);
    }

    public void ActiveGrid(bool a_bActive)
    {
        GameObject goScreen = Child("Screen", 0);
        goScreen.SetActive(a_bActive);
        Child(goScreen, 0).SetActive(SVariables.s_eConfiguration.isVR());
        Child(goScreen, 1).SetActive(SVariables.s_eConfiguration.isVR());
        Child(goScreen, 2).SetActive(!SVariables.s_eConfiguration.isVR());
    }

    protected void EnablePhotodiodeDrawing(bool a_bEnable)
    {
        ActiveGrid(true);
        // Could be one or two depending on if configuration is VR or not
        SGrid[] aGrids = GameObject.FindObjectsByType<SGrid>(FindObjectsSortMode.None);
        foreach (SGrid grid in aGrids)
            grid.m_bDrawPhotodiode = a_bEnable;
    }

    protected Vector2 Vector2TargetCoord(Vector2 a_v2Position)
    {
        float x = (a_v2Position.x - SVariables.GetMj()) / SVariables.s_fUj;
        float y = (100 - a_v2Position.y - SVariables.GetMi()) / SVariables.s_fUi;
        return new Vector2(x, y);
    }

    protected bool isThereOnlyOnePossibleTargetOnScreen()
    {
        return (SVariables.s_eOrientation.isReverted() && SVariables.s_iNumCol == 1) ||
                (SVariables.s_eOrientation.isNotReverted() && SVariables.s_iNumRow == 1) ||
                (SVariables.s_iNumCol == 1 && SVariables.s_iNumRow == 1);
    }

    protected static GameObject Child(GameObject a_oGameObject, int a_iChild)
    {
        return a_oGameObject.transform.GetChild(a_iChild).gameObject;
    }

    protected static GameObject Child(string a_sGameObject, int a_iChild)
    {
        return GameObject.Find(a_sGameObject).transform.GetChild(a_iChild).gameObject;
    }

    protected static T Component<T>(string a_sGameObject)
    {
        return GameObject.Find(a_sGameObject).GetComponent<T>();
    }
}
