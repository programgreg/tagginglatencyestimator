//source: https://answers.unity.com/questions/489942/how-to-make-a-readonly-property-in-inspector.html

using UnityEngine;

namespace Inspector
{
    public class ReadOnlyAttribute : PropertyAttribute { }
}