//source: https://www.reddit.com/r/Unity3D/comments/1s6czv/inspectorbutton_add_a_custom_button_to_your/

using UnityEngine;

[System.AttributeUsage(System.AttributeTargets.Field)]
public class ButtonAttribute : PropertyAttribute
{
    public static float kDefaultButtonWidth = 80;

    public readonly string MethodName;

    private float _buttonWidth = kDefaultButtonWidth;
    public float ButtonWidth
    {
        get { return _buttonWidth; }
        set { _buttonWidth = value; }
    }

    public ButtonAttribute(string MethodName)
    {
        this.MethodName = MethodName;
    }
}