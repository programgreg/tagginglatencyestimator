﻿using System.Collections;
using System.Collections.Generic;
using UCoreNet;
using UnityEngine;
using UnityEngine.UI;


public class SResult : SModel
{

    private Text a_textResult;
    private string txt = "";

    private float std, photo1, latency;

    void OnEnable()
    {
        a_textResult = Component<Text>("Results");
        float latency1 = 0, std1 = 0;
        if (SVariables.s_bCompareWithOtherConfiguration)
        {
            std1 = std;
            latency1 = latency;
        }
        else
        {
            photo1 = SVariables.s_fDelayPhotodiodeMs;
        }
        float e;
        latency = SVariables.GetLatency(out std, out e);


        txt = "";
        if (SVariables.s_eConfiguration.isVR())
        {
            txt += "The screen was split in two. ";
        }
        else if (SVariables.s_eConfiguration == SVariables.Configuration.LOFAP)
        {
            txt += "The screen is split into multiple part, each one rendered by a different camera. ";
            txt += "The number of cameras is not known or not necessary for estimating the latency as you know the delay of the first rendered camera";
            txt += ", measured thanks to a photodiode. ";
        }

        if (SVariables.s_eOrientation.isReverted())
        {
            txt += "The screen was turned to 90°. ";
            if (SVariables.s_fColPhotodiode == INVALID)
            {
                txt += "The position of the photodiode is not known and was estimated in order to maximize the standard deviation. ";
            }
        }
        else if (SVariables.s_eOrientation.isNotReverted() && SVariables.s_fRowPhotodiode == INVALID)
        {
            txt += "The position of the photodiode is not known and was estimated in order to maximize the standard deviation. ";
        }
        else if (SVariables.s_eOrientation.isUnknown())
        {
            txt += "The orientation of the screen is unknown. ";
            if (SVariables.s_fColPhotodiode == INVALID || SVariables.s_fRowPhotodiode == INVALID)
            {
                txt += "The position of the photodiode is not known and was estimated in order to maximize the standard deviation. ";
            }
        }

        if (SVariables.s_iNumberOfTarget == 1)
        {
            txt += "The number of target was set to one, which implies a lot of variability in the location of the barycenter. ";
        }

        if (SVariables.s_bUniformDistribution)
        {
            txt += "The targets were uniformly distributed on the screen. ";
        }
        else
        {
            txt += "The distribution of the target on the screens is not uniform. ";
        }

        if (e < 0)
        {
            txt += "The latency of the photodiode is unknown. ";
            txt += "An error of " + Format(Mathf.Abs(latency)) + " ms are commited by the difference in position between the photodiode and the barycenter of the targets. ";
            if (latency > 0)
            {
                txt += "The stimulus observed by the photodiode appears earlier on screen than the barycenter of the targets. ";
            }
            else if (latency < 0)
            {
                txt += "The stimulus observed by the photodiode appears later on screen than the barycenter of the targets. ";
            }
            else
            {
                txt += "The stimulus observed by the photodiode coincide with the barycenter of the targets on screen. ";
            }
            txt += "Thus, the target was probably observed by the user AT LEAST " + Format(latency) + " ms earlier than the onset of the tag. ";
            txt += "However, this latency cannot be precisely known. ";
            txt += "Comparison between different experimental conditions are still possible if the screen orientation and configuration was the same in all the experimental conditions. ";

        }
        else
        {
            txt += "The latency of the photodiode is " + Format(SVariables.s_fDelayPhotodiodeMs) + ". ";
            txt += Format(e) + " ms are specific to the plateform you use, and are not induced by the time taken to display the pixels on the screen. ";
            txt += "An error of " + Format(Mathf.Abs(latency)) + " ms are commited by the difference in position between the photodiode and the barycenter of the targets. ";
            if (latency > 0)
            {
                txt += "The stimulus observed by the photodiode appears earlier on screen than the barycenter of the targets. ";
                txt += "Thus, the mean target was probably observed by the user " + Format(SVariables.s_fDelayPhotodiodeMs + latency) + " ms earlier than the onset of the tag. ";
            }
            else if (latency < 0)
            {
                txt += "The stimulus observed by the photodiode appears later on screen than the barycenter of the targets. ";
                txt += "Thus, the mean target was probably observed by the user " + Format(SVariables.s_fDelayPhotodiodeMs + latency) + " ms earlier than the onset of the tag. ";
            }
            else
            {
                txt += "The stimulus observed by the photodiode coincide with the barycenter of the targets on screen. ";
                txt += "Thus, the mean target was probably observed by the user " + Format(SVariables.s_fDelayPhotodiodeMs) + " ms earlier than the onset of the tag. ";
            }

        }

        txt += "There is a standard deviation about " + Format(std) + " ms which take into account the variability in the estimation of the photodiode, barycenter, and eventually the graphical interactions in this software. ";

        txt += "On the assemption that the mean ERP is determined with a substantial number of epoch, we consider that the jitter due to the tagging is equals to zero. ";
        if (SVariables.s_fDelayPhotodiodeMs != INVALID)
        {
            txt += "It means that a ''normal'' latency should be in the range [" + Format(SVariables.s_fDelayPhotodiodeMs + latency - std) + "; ";
            txt += Format(SVariables.s_fDelayPhotodiodeMs + latency + std) + "] ms. ";
        }

        if (SVariables.s_bCompareWithOtherConfiguration)
        {
            if (photo1 == INVALID || SVariables.s_fDelayPhotodiodeMs == INVALID)
            {
                txt += "\nIn the previous condition, the error commited by the difference between the position of the photodiode and the mean target was ";
                txt += Format(Mathf.Abs(latency1)) + "(" + Format(std1) + "). ";
                txt += "If the screen configuration and orientation was the same in the two conditions, a difference in latency in the range [";
                txt += Format(Mathf.Abs(latency - latency1) - std - std1) + "; ";
                txt += Format(Mathf.Abs(latency - latency1) + std + std1) + "] is observable. ";
            }
            else
            {
                txt += "\nIn the previous condition, the latency (sd) was ";
                txt += Format(Mathf.Abs(photo1 + latency1)) + "(" + Format(std1) + "). ";
                txt += "Comparing the two experimental conditions, a difference in latency in the range [";
                txt += Format(Mathf.Abs(SVariables.s_fDelayPhotodiodeMs + latency - photo1 - latency1) - std - std1) + "; ";
                txt += Format(Mathf.Abs(SVariables.s_fDelayPhotodiodeMs + latency - photo1 - latency1) + std + std1) + "] is observable. ";
            }


        }

        a_textResult.text = txt;
    }

    private string Format(float f, int precision = 2)
    {
        string[] sArray = f.ToString().Split(",");
        if (sArray.Length == 1)
        {
            return sArray[0];
        }
        else
        {
            return sArray[0] + "," + sArray[1].Substring(0, precision);
        }
    }



    public void Finish()
    {
        GUIUtility.systemCopyBuffer = txt;
        if (SVariables.s_bCompareWithOtherConfiguration)
#if UNITY_EDITOR

            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        else
            GoToNextStep();
    }
}
