using UnityEngine;
public static class SCommon
{
    public static readonly float s_fRefreshScreenTime = 16f;

    /**
    * a_fHeight is expressed in number of items. Example:
    * Let' s say you have 2 items on the screen spaced by half of the screen
    * and you want to know the time needed to refresh the first item
    * then you should set a_fHeight to 1 and a_fU to 50 or 0.5
    **/
    public static float PScR(float a_fHeight, float a_fU)
    {
        //Normalize Uj between 0 and 1 if given as percent
        if (a_fU > 1)
            a_fU /= 100f;
        return s_fRefreshScreenTime * a_fU * a_fHeight;
    }

    public static float GetM(float a_fScreenDim, float a_iNumItems, float a_fU)
    {
        return (a_fScreenDim - (a_iNumItems - 1) * a_fU) / 2f;
    }

    //a_fCoordPhotodiode: item index start at 0
    public static float GetPhotodiode(float a_fM, float a_fU, float a_fCoordPhotodiode)
    {
        return a_fM + a_fU * a_fCoordPhotodiode;
    }

    private static void ComputeBarycentersSTD(float a_iNumTrials, float[] a_afBarycentersI, float[] a_afBarycentersJ, float a_fBarycenterI, float a_fBarycenterJ, out float a_fBarycenterStdI, out float a_fBarycenterStdJ)
    {
        a_fBarycenterStdI = 0; a_fBarycenterStdJ = 0;
        for (int i = 0; i < a_iNumTrials; ++i)
        {
            a_fBarycenterStdI += Mathf.Pow(a_afBarycentersI[i] - a_fBarycenterI, 2);
            a_fBarycenterStdJ += Mathf.Pow(a_afBarycentersJ[i] - a_fBarycenterJ, 2);
        }
        a_fBarycenterStdI = Mathf.Sqrt(a_fBarycenterStdI / (float)a_iNumTrials);
        a_fBarycenterStdJ = Mathf.Sqrt(a_fBarycenterStdJ / (float)a_iNumTrials);
    }

    private static void ComputeBarycenter(System.Random a_oRandomGenerator, int a_iNumTargets, int a_iNumRow, int a_iNumCol, out float a_fBaryI, out float a_fBaryJ)
    {
        a_fBaryI = 0;
        a_fBaryJ = 0;
        for (int j = 0; j < a_iNumTargets; ++j)
        {
            a_fBaryI += (float)a_oRandomGenerator.Next(0, a_iNumRow);
            a_fBaryJ += (float)a_oRandomGenerator.Next(0, a_iNumCol);
        }
        a_fBaryI /= (float)a_iNumTargets;
        a_fBaryJ /= (float)a_iNumTargets;
    }

    public static void ComputeBarycenters(int a_iNumTrials, int a_iNumTargets, int a_iNumRow, int a_iNumCol, out float a_fBaryI, out float a_fBaryJ, out float a_fMeanStdI, out float a_fMeanStdJ)
    {
        System.Random oRandomGenerator = new System.Random();
        float[] afBarycentersI = new float[a_iNumTrials];
        float[] afBarycentersJ = new float[a_iNumTrials];
        float fbaryI, fbaryJ;
        a_fBaryI = 0; a_fBaryJ = 0;
        for (int i = 0; i < a_iNumTrials; ++i)
        {
            ComputeBarycenter(oRandomGenerator, a_iNumTargets, a_iNumRow, a_iNumCol, out fbaryI, out fbaryJ);
            afBarycentersI[i] = fbaryI;
            afBarycentersJ[i] = fbaryJ;
            a_fBaryI += fbaryI;
            a_fBaryJ += fbaryJ;
        }
        a_fBaryI /= (float)a_iNumTrials;
        a_fBaryJ /= (float)a_iNumTrials;
        SCommon.ComputeBarycentersSTD(a_iNumTrials, afBarycentersI, afBarycentersJ, a_fBaryI, a_fBaryJ, out a_fMeanStdI, out a_fMeanStdJ);
    }
}