using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
 
public class STooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private GameObject m_oTooltip;

    void Start()
    {
        m_oTooltip = transform.Find("Tooltip").gameObject;
        m_oTooltip.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        m_oTooltip.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        m_oTooltip.SetActive(false);
    }
}