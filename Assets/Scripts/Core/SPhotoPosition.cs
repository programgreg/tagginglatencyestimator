﻿using UnityEngine;

public class SPhotoPosition : SModel
{

    private bool m_bPhotodiodeSet;

    void OnEnable()
    {
        EnablePhotodiodeDrawing(true);
        m_bPhotodiodeSet = false;
    }

    public void SetIDontKnow()
    {
        SVariables.s_fRowPhotodiode = INVALID;
        SVariables.s_fColPhotodiode = INVALID;
        GoToNextStep();
    }

    public void SetPhotodiodePosition(Vector2 m_v2Position)
    {
        Vector2 v2Transformed = Vector2TargetCoord(m_v2Position);
        SVariables.s_fColPhotodiode = v2Transformed.x;
        SVariables.s_fRowPhotodiode = v2Transformed.y;
        m_bPhotodiodeSet = true;
    }

    public void Validate()
    {
        if(m_bPhotodiodeSet)
        {
            GoToNextStep();
        }
    }

}
