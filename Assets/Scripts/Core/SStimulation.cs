﻿using System;
using UnityEngine.UI;

public class SStimulation : SModel
{

    InputField m_oColumn;
    InputField m_oRow;

    void OnEnable()
    {
        ActiveGrid(false);
        m_oColumn = Component<InputField>("Column");
        m_oRow = Component<InputField>("Row");
        m_oRow.text = "";
        m_oColumn.text = "";
        m_oColumn.interactable = !SVariables.s_eOrientation.isReverted();
        m_oRow.interactable = !SVariables.s_eOrientation.isNotReverted();
        if (SVariables.s_eOrientation.isReverted())
            SVariables.s_iNumRow = 1;
        if (SVariables.s_eOrientation.isNotReverted())
            SVariables.s_iNumCol = 1;
    }

    public void Validate()
    {
        string row = m_oRow.text;
        string col = m_oColumn.text;
        if (row != "" && SVariables.s_eOrientation.isReverted())
        {
            SetCol();
            GoToNextStep();
        }
        else if (col != "" && SVariables.s_eOrientation.isNotReverted())
        {
            SetRow();
            GoToNextStep();
        }
    }

    public void SetRow()
    {
        string col = m_oColumn.text;
        if (col != "")
            SVariables.s_iNumRow = int.Parse(col);
    }

    public void SetCol()
    {
        string row = m_oRow.text;
        if (row != "")
            SVariables.s_iNumCol = int.Parse(row);
    }
}
