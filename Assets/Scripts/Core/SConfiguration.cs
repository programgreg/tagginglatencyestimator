﻿public class SConfiguration : SModel
{

    public void SetVR()
    {
        SVariables.s_eConfiguration = SVariables.Configuration.VR;
        GoToNextStep();
    }

    public void SetPC()
    {
        SVariables.s_eConfiguration = SVariables.Configuration.NO_VR;
        GoToNextStep();
    }

    public void SetLOFAP()
    {
        SVariables.s_eConfiguration = SVariables.Configuration.LOFAP;
        GoToNextStep();
    }
}
