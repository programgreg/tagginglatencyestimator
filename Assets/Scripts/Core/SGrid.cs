﻿using UCoreNet;
using UnityEngine;

public class SGrid : MonoBehaviour
{

    UCoreNet.Canvas m_canvas;
    public bool m_bDrawPhotodiode = false;
    private Vector2 m_v2PhotoDiodePosition = new Vector2(-1, -1);

    // Warning: position is reinitialised to (-1, -1) after each click outside the grid
    private bool IsPhotodiodePositionSet()
    {
        return m_v2PhotoDiodePosition.x != -1 && m_v2PhotoDiodePosition.y != -1;
    }

    void OnPostRender()
    {
        GLLib.InitRendering();

        m_canvas = new UCoreNet.Canvas(GLLib.s_world, new Vector2(0f, 0f),
                                       new Vector2(1f, 1f), new Vector2(0, 0), new Vector2(100, 100));
        GLLib.DrawGridIntersections(SVariables.s_iNumCol, SVariables.s_iNumRow,
                                    m_canvas, Color.red, new Vector2(SVariables.GetMj(), SVariables.GetMi()));

        m_canvas.DrawFrame(Color.black);

        if(IsPhotodiodePositionSet())
            GLLib.DrawCross(m_v2PhotoDiodePosition - new Vector2(2, 2),
                            m_v2PhotoDiodePosition + new Vector2(2, 2), m_canvas, Color.black);

        GLLib.CloseRendering();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && m_bDrawPhotodiode)
        {
            Vector3 pos = Input.mousePosition;
            Vector3 posInPlot = GLLib.ConvertScreenCoordinate(pos, m_canvas, GetComponent<Camera>());
            if (m_canvas.IsIn(posInPlot))
            {
                m_v2PhotoDiodePosition = posInPlot;
                SPhotoPosition sp = GameObject.FindFirstObjectByType<SPhotoPosition>();
                SDistribution sd = GameObject.FindFirstObjectByType<SDistribution>();
                if (name == "Right")
                {
                    if (sp != null)
                        sp.SetPhotodiodePosition(m_v2PhotoDiodePosition + new Vector2(100, 0));
                    else
                        sd.SetTarget(m_v2PhotoDiodePosition);
                }
                else
                {
                    if (sp != null)
                        sp.SetPhotodiodePosition(m_v2PhotoDiodePosition);
                    else
                        sd.SetTarget(m_v2PhotoDiodePosition);
                }
            }
            else
            {
                m_v2PhotoDiodePosition = new Vector2(-1, -1);
            }
        }
    }
}
