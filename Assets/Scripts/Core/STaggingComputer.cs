﻿
using Inspector;
using UnityEngine;

/**
* Wrapper around `common.py`, to facilitate computation of tagging latency.
* See the Python version (.\PythonProject\tle\tagging_computer.py) for the detailed documentation.
* Or consults the markdown doc (.\doc\tagging_computer.md)
**/
public class STaggingComputer : MonoBehaviour
{

    // This is hack to display a checkbox in the UI
    [Button("Compute")]
    public bool compute;

    // Parameters
    public int numberOfTrialForBarycenterComputation = 10000;
    public int screenWidthInPixels = 890;
    public int screenHeightInPixels = 500;
    public int Ui = 87;
    public int Uj = 166;
    public int itemRowCoordWhereWasLocatedThePhotodiode = 2;
    public int itemColCoordWhereWasLocatedThePhotodiode = 2;
    public int numberOfDisplayedTargets = 12;
    public int itemNumberInACol = 6;
    public int itemNumberInARow = 6;
    public float latencyMeasuredByPhotodiode = 38f;

    //to compute
    [ReadOnly]
    public float barycenterI;

    [ReadOnly]
    public float barycenterJ;

    [ReadOnly]
    public float barycenterStdI;

    [ReadOnly]
    public float barycenterStdJ;

    [ReadOnly]
    public float Mi;

    [ReadOnly]
    public float Mj;

    [ReadOnly]
    public float photodiodeX;

    [ReadOnly]
    public float photodiodeY;

    [ReadOnly]
    public float xLatency;

    [ReadOnly]
    public float yLatency;

    [ReadOnly]
    public float xLatencyStd;

    [ReadOnly]
    public float yLatencyStd;

    [ReadOnly]
    public float platformLatencyX;

    [ReadOnly]
    public float platformLatencyY;

    [ReadOnly]
    public float maxLatency;

    void SetM()
    {
        Mi = SCommon.GetM(screenHeightInPixels, itemNumberInACol, Ui);
        Mj = SCommon.GetM(screenWidthInPixels, itemNumberInARow, Uj);
    }

    float GetPhotodiodeY(int a_I)
    {
        return SCommon.GetPhotodiode(Mi, Ui, a_I) / (float)screenHeightInPixels;
    }

    float GetPhotodiodeX(int a_J)
    {
        return SCommon.GetPhotodiode(Mj, Uj, a_J) / (float)screenHeightInPixels;
    }

    void ComputeBarycenters()
    {
        SCommon.ComputeBarycenters(numberOfTrialForBarycenterComputation, numberOfDisplayedTargets, itemNumberInACol, itemNumberInARow, out barycenterI, out barycenterJ, out barycenterStdI, out barycenterStdJ);
    }

    float PScRX(float h)
    {
        float UjNormalized = (float)Uj / (float)screenWidthInPixels;
        return SCommon.PScR(h, UjNormalized);
    }

    float PScRY(float h)
    {
        float UiNormalized = (float)Ui / (float)screenHeightInPixels;
        return SCommon.PScR(h, UiNormalized);
    }

    void Start()
    {
        Compute();
    }

    void Compute()
    {
        SetM();
        ComputeBarycenters();
        photodiodeX = GetPhotodiodeX(itemColCoordWhereWasLocatedThePhotodiode);
        photodiodeY = GetPhotodiodeY(itemRowCoordWhereWasLocatedThePhotodiode);
        xLatency = PScRX((float)(barycenterJ - itemColCoordWhereWasLocatedThePhotodiode));
        yLatency = PScRY((float)(barycenterI - itemRowCoordWhereWasLocatedThePhotodiode));
        xLatencyStd = PScRX(barycenterStdJ);
        yLatencyStd = PScRY(barycenterStdI);
        platformLatencyX = latencyMeasuredByPhotodiode - PScRX(itemColCoordWhereWasLocatedThePhotodiode);
        platformLatencyY = latencyMeasuredByPhotodiode - PScRY(itemRowCoordWhereWasLocatedThePhotodiode);
        maxLatency = xLatency + xLatencyStd - yLatency - yLatencyStd;
    }

}
