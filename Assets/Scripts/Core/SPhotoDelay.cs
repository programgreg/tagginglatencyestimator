﻿using UnityEngine.UI;

public class SPhotoDelay : SModel
{
    private InputField m_oDelay;

    void OnEnable()
    {
        m_oDelay = Component<InputField>("InputField");
        m_oDelay.text = "";
        ActiveGrid(false);
    }

    public void Validate()
    {
        string val = m_oDelay.text;
        if(val != "")
            GoToNextStep();
    }

    public void SetDelay()
    {
        string val = m_oDelay.text;
        if (val != "")
            SVariables.s_fDelayPhotodiodeMs = float.Parse(val);
    }

    public void SetIDontKnow()
    {
        SVariables.s_fDelayPhotodiodeMs = INVALID;
        GoToNextStep();
    }

}
