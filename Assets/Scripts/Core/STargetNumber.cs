﻿using UnityEngine.UI;

public class STargetNumber : SModel
{
    private InputField m_oNumber;

    void OnEnable()
    {
        m_oNumber = Component<InputField>("InputField");
        m_oNumber.text = "";
        ActiveGrid(false);
        if (isThereOnlyOnePossibleTargetOnScreen())
            Pass();
    }

    public void Validate()
    {
        string sNumber = m_oNumber.text;
        if (sNumber != "")
        {
            GoToNextStep();
        }
    }

    public void SetNumber()
    {
        string sNumber = m_oNumber.text;
        if (sNumber != "")
        {
            SVariables.s_iNumberOfTarget = int.Parse(sNumber);
        }
    }

    public void Pass()
    {
        SVariables.s_iNumberOfTarget = 1;
        GoToNextStep();
    }


}
