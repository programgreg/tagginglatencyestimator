﻿using UnityEngine;

public static class SVariables
{
    public static readonly int INVALID = -1;

    public enum Configuration
    {
        VR,
        NO_VR,
        LOFAP
    }

    public static bool isVR(this Configuration configuration)
    {
        return configuration == Configuration.VR;
    }

    public enum Orientation
    {
        REVERTED,
        NOT_REVERTED,
        UNKNOWN
    }

    public static bool isReverted(this Orientation orientation)
    {
        return orientation == Orientation.REVERTED;
    }

    public static bool isNotReverted(this Orientation orientation)
    {
        return orientation == Orientation.NOT_REVERTED;
    }

    public static bool isUnknown(this Orientation orientation)
    {
        return orientation == Orientation.UNKNOWN;
    }

    public static Configuration s_eConfiguration;

    public static Orientation s_eOrientation;

    public static int s_iNumRow = INVALID;

    public static int s_iNumCol = INVALID;

    public static int s_iNumberOfTarget = 12;

    public static int s_iNumTrials = 10000;

    // Ui and Uj are expressed in screen percents
    public static float s_fUi;
    public static float s_fUj;

    public static bool s_bUniformDistribution;

    public static float s_fRowPhotodiode;
    public static float s_fColPhotodiode;
    public static float s_fDelayPhotodiodeMs;

    public static bool s_bCompareWithOtherConfiguration;
    public static float[] s_afResult1;
    public static float[] s_afResult2;
    public static float[] s_afResult3;

    public static float s_fBaryI = INVALID;
    public static float s_fBaryJ = INVALID;

    public static float s_fGraphicalStdX = 0.01f;
    public static float s_fGraphicalStdY = 0.1f;

    public static float GetMi()
    {
        return SCommon.GetM(100, s_iNumRow, s_fUi);
    }

    public static float GetMj()
    {
        return SCommon.GetM(100, s_iNumCol, s_fUj);
    }

    public static float PhotodiodeY()
    {
        return SCommon.GetPhotodiode(GetMi(), s_fUi, s_fRowPhotodiode);
    }

    public static float PhotodiodeX()
    {
        return SCommon.GetPhotodiode(GetMj(), s_fUj, s_fColPhotodiode);
    }

    private static float PScRX(float h)
    {
        return SCommon.PScR(h, s_fUj);
    }

    private static float PScRY(float h)
    {
        return SCommon.PScR(h, s_fUi);
    }

    private static float GetLatencyWithUnknownOrientation(out float a_fStd, out float a_fPlatformSpecificLatency)
    {
        s_eOrientation = Orientation.REVERTED;
        float std1, e1;
        float latency1 = GetLatency(out std1, out e1);
        s_eOrientation = Orientation.NOT_REVERTED;
        float std2, e2;
        float latency2 = GetLatency(out std2, out e2);
        s_eOrientation = Orientation.UNKNOWN;
        if (latency1 + std1 > latency2 + std2)
        {
            a_fStd = std1;
            a_fPlatformSpecificLatency = e1;
            return latency1;
        }
        else
        {
            a_fStd = std2;
            a_fPlatformSpecificLatency = e2;
            return latency2;
        }
    }

    private static float GetLatencyForNotRevertedScreenAndUniformDistributionWithValidPhotodiode(out float a_fStd, out float a_fPlatformSpecificLatency)
    {
        float fStdI, fStdJ;
        if (s_fBaryI == INVALID)
            ComputeBarycenters(out s_fBaryI, out s_fBaryJ, out fStdI, out fStdJ);
        else
            fStdI = s_fGraphicalStdY;
        a_fPlatformSpecificLatency = s_fDelayPhotodiodeMs - PScRY(s_fRowPhotodiode);
        a_fStd = PScRY(fStdI + s_fGraphicalStdY);
        return PScRY(s_fBaryI - s_fRowPhotodiode);
    }

    private static float GetLatencyForNotRevertedScreenAndUnknownDistributionWithValidPhotodiode(out float a_fStd, out float a_fPlatformSpecificLatency)
    {
        float fStdI;
        if (Mathf.Abs(s_fRowPhotodiode - s_iNumRow + 1) > s_fRowPhotodiode)
        {
            s_fBaryI = (s_fRowPhotodiode + s_iNumRow - 1) / 2f;
            fStdI = Mathf.Abs(s_fBaryI - s_fRowPhotodiode);
        }
        else
        {
            s_fBaryI = s_fRowPhotodiode / 2f;
            fStdI = s_fRowPhotodiode / 2f;
        }
        a_fPlatformSpecificLatency = s_fDelayPhotodiodeMs - PScRY(s_fRowPhotodiode);
        a_fStd = PScRY(fStdI + s_fGraphicalStdY);
        return PScRY(s_fBaryI - s_fRowPhotodiode);
    }

    private static float GetLatencyForNotRevertedScreenAndUniformDistributionWithUnvalidPhotodiode(out float a_fStd, out float a_fPlatformSpecificLatency)
    {
        float fStdI, fStdJ;
        if (s_fBaryI == INVALID)
            ComputeBarycenters(out s_fBaryI, out s_fBaryJ, out fStdI, out fStdJ);
        else
            fStdI = s_fGraphicalStdY;
        if (Mathf.Abs(s_fBaryI - s_iNumRow + 1) > s_fBaryI)
        {
            s_fRowPhotodiode = (s_fBaryI + s_iNumRow - 1) / 2f;
            fStdI += Mathf.Abs(s_fBaryI - s_fRowPhotodiode);
        }
        else
        {
            s_fRowPhotodiode = s_fBaryI / 2f;
            fStdI += s_fBaryI / 2f;
        }
        a_fPlatformSpecificLatency = s_fDelayPhotodiodeMs - PScRY(s_fRowPhotodiode);
        a_fStd = PScRY(fStdI);
        return PScRY(s_fBaryI - s_fRowPhotodiode);
    }

    private static float GetLatencyForNotRevertedScreenAndUnknownDistributionWithUnvalidPhotodiode(out float a_fStd, out float a_fPlatformSpecificLatency)
    {
        s_fRowPhotodiode = (s_iNumRow - 1) / 2f;
        s_fBaryI = s_fRowPhotodiode;
        a_fStd = 8;
        a_fPlatformSpecificLatency = s_fDelayPhotodiodeMs - PScRY(s_fRowPhotodiode);
        return 0;
    }

    private static float GetLatencyForNotRevertedScreen(out float a_fStd, out float a_fPlatformSpecificLatency)
    {
        if (s_fRowPhotodiode != INVALID)
        {
            if (s_bUniformDistribution)
            {
                return GetLatencyForNotRevertedScreenAndUniformDistributionWithValidPhotodiode(out a_fStd, out a_fPlatformSpecificLatency);
            }
            else
            {
                return GetLatencyForNotRevertedScreenAndUnknownDistributionWithValidPhotodiode(out a_fStd, out a_fPlatformSpecificLatency);
            }
        }
        else
        {
            if (s_bUniformDistribution)
            {
                return GetLatencyForNotRevertedScreenAndUniformDistributionWithUnvalidPhotodiode(out a_fStd, out a_fPlatformSpecificLatency);
            }
            else
            {
                return GetLatencyForNotRevertedScreenAndUnknownDistributionWithUnvalidPhotodiode(out a_fStd, out a_fPlatformSpecificLatency);
            }
        }

    }

    private static float GetLatencyForRevertedScreenAndUniformDistributionWithValidPhotodiode(out float a_fStd, out float a_fPlatformSpecificLatency)
    {
        float fStdI, fStdJ;
        if (s_fBaryJ == INVALID)
            ComputeBarycenters(out s_fBaryI, out s_fBaryJ, out fStdI, out fStdJ);
        else
            fStdJ = s_fGraphicalStdX;
        a_fPlatformSpecificLatency = s_fDelayPhotodiodeMs - PScRX(s_fColPhotodiode);
        a_fStd = PScRX(fStdJ + s_fGraphicalStdX);
        return PScRX(s_fBaryJ - s_fColPhotodiode);
    }

    private static float GetLatencyForRevertedScreenAndUnknownDistributionWithValidPhotodiode(out float a_fStd, out float a_fPlatformSpecificLatency)
    {
        float fStdJ;
        if (Mathf.Abs(s_fColPhotodiode - s_iNumCol + 1) > s_fColPhotodiode)
        {
            s_fBaryJ = (s_iNumCol - 1 + s_fColPhotodiode) / 2f;
            fStdJ = Mathf.Abs(s_fBaryJ - s_fColPhotodiode);
        }
        else
        {
            s_fBaryJ = s_fColPhotodiode / 2f;
            fStdJ = s_fColPhotodiode / 2f;
        }
        a_fPlatformSpecificLatency = s_fDelayPhotodiodeMs - PScRX(s_fColPhotodiode);
        a_fStd = PScRX(fStdJ + s_fGraphicalStdX);
        return PScRX(s_fBaryJ - s_fColPhotodiode);
    }

    private static float GetLatencyForRevertedScreenAndUniformDistributionWithUnvalidPhotodiode(out float a_fStd, out float a_fPlatformSpecificLatency)
    {
        float fStdI, fStdJ;
        if (s_fBaryJ == INVALID)
            ComputeBarycenters(out s_fBaryI, out s_fBaryJ, out fStdI, out fStdJ);
        else
            fStdJ = s_fGraphicalStdX;
        if (Mathf.Abs(s_fBaryJ - s_iNumCol + 1) > s_fBaryJ)
        {
            s_fColPhotodiode = (s_fBaryJ + s_iNumCol - 1) / 2f;
            fStdJ += Mathf.Abs(s_fBaryJ - s_fColPhotodiode);
        }
        else
        {
            s_fColPhotodiode = s_fBaryJ / 2f;
            fStdJ += s_fBaryJ / 2f;
        }
        a_fPlatformSpecificLatency = s_fDelayPhotodiodeMs - PScRX(s_fColPhotodiode);
        a_fStd = PScRX(fStdJ);
        return PScRX(s_fBaryJ - s_fColPhotodiode);
    }

    private static float GetLatencyForRevertedScreenAndUnknownDistributionWithUnvalidPhotodiode(out float a_fStd, out float a_fPlatformSpecificLatency)
    {
        s_fColPhotodiode = (s_iNumCol - 1) / 2f;
        s_fBaryJ = s_fColPhotodiode;
        a_fStd = 8;
        a_fPlatformSpecificLatency = s_fDelayPhotodiodeMs - PScRX(s_fColPhotodiode);
        return 0;
    }

    private static float GetLatencyForRevertedScreen(out float a_fStd, out float a_fPlatformSpecificLatency)
    {

        if (s_fColPhotodiode != INVALID)
        {
            if (s_bUniformDistribution)
            {
                return GetLatencyForRevertedScreenAndUniformDistributionWithValidPhotodiode(out a_fStd, out a_fPlatformSpecificLatency);
            }
            else
            {
                return GetLatencyForRevertedScreenAndUnknownDistributionWithValidPhotodiode(out a_fStd, out a_fPlatformSpecificLatency);
            }
        }
        else
        {
            if (s_bUniformDistribution)
            {
                return GetLatencyForRevertedScreenAndUniformDistributionWithUnvalidPhotodiode(out a_fStd, out a_fPlatformSpecificLatency);
            }
            else
            {
                return GetLatencyForRevertedScreenAndUnknownDistributionWithUnvalidPhotodiode(out a_fStd, out a_fPlatformSpecificLatency);
            }
        }
    }

    public static float GetLatency(out float a_fStd, out float a_fPlatformSpecificLatency)
    {
        if (s_eOrientation.isUnknown())
        {
            return GetLatencyWithUnknownOrientation(out a_fStd, out a_fPlatformSpecificLatency);
        }
        if (s_eOrientation.isNotReverted())
        {
            return GetLatencyForNotRevertedScreen(out a_fStd, out a_fPlatformSpecificLatency);
        }
        else //if(s_eOrientation == Orientation.REVERTED)
        {
            return GetLatencyForRevertedScreen(out a_fStd, out a_fPlatformSpecificLatency);
        }
    }

    private static void ComputeBarycenters(out float a_fBarycenterI, out float a_fBarycenterJ, out float a_fBarycenterStdI, out float a_fBarycenterStdJ)
    {
        SCommon.ComputeBarycenters(s_iNumTrials, s_iNumberOfTarget, s_iNumRow, s_iNumCol, out a_fBarycenterI, out a_fBarycenterJ, out a_fBarycenterStdI, out a_fBarycenterStdJ);
    }
}
