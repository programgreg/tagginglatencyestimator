﻿using UnityEngine;
using UnityEngine.UI;

public class SDistribution : SModel
{
    private GameObject m_oScreenDist;
    private GameObject m_oPass;
    private GameObject m_oValidate;
    private GameObject m_oYes;
    private GameObject m_oNo;
    private Text m_oQuestion;
    private bool m_bTargetSet;

    void OnEnable()
    {
        m_oScreenDist = GameObject.Find("Screen_Distribution");
        m_oYes = Child(m_oScreenDist, 1);
        m_oNo = Child(m_oScreenDist, 2);
        m_oPass = Child(m_oScreenDist, 3);
        m_oValidate = Child(m_oScreenDist, 4);
        m_oQuestion = Child(m_oScreenDist, 0).GetComponent<Text>();
        m_bTargetSet = false;
        if (SVariables.s_iNumberOfTarget == 1)
        {
            if (isThereOnlyOnePossibleTargetOnScreen())
            {
                SVariables.s_fBaryJ = 0;
                SVariables.s_fBaryI = 0;
                GoToNextStep();
            }
            else
            {
                SVariables.s_bUniformDistribution = true;
                GetTargetLocation();
            }
        }
        else
        {
            GetDistribution();
        }
    }

    public void Validate()
    {
        if(m_bTargetSet)
            GoToNextStep();
    }

    private void GetDistribution()
    {
        m_oQuestion.text = "Are the targets uniformly distributed on screen?";
        m_oPass.SetActive(false);
        m_oValidate.SetActive(false);
        m_oYes.SetActive(true);
        m_oNo.SetActive(true);
        ActiveGrid(false);
    }

    private void GetTargetLocation()
    {
        m_oQuestion.text = "Where is the target?";
        m_oYes.SetActive(false);
        m_oNo.SetActive(false);
        m_oPass.SetActive(true);
        m_oPass.SetActive(true);
        ActiveGrid(true);
        EnablePhotodiodeDrawing(true);
    }

    public void SetTarget(Vector2 m_v2Position)
    {
        Vector2 v2Transformed = Vector2TargetCoord(m_v2Position);
        SVariables.s_fBaryJ = v2Transformed.x;
        SVariables.s_fBaryI = v2Transformed.y;
        m_bTargetSet = true;
    }

    public void SetYes()
    {
        SVariables.s_bUniformDistribution = true;
        GoToNextStep();
    }

    public void SetNo()
    {
        SVariables.s_bUniformDistribution = false;
        GoToNextStep();
    }

}
