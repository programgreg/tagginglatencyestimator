using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class TestSuite : IPrebuildSetup
{

    [SetUp]
    public void Setup()
    {
        SVariables.s_eConfiguration = SVariables.Configuration.VR;
        SVariables.s_eOrientation = SVariables.Orientation.REVERTED;
        SVariables.s_iNumRow = 6;
        SVariables.s_iNumCol = 6;
        SVariables.s_iNumTrials = 10000;
        SVariables.s_iNumberOfTarget = 12;
        SVariables.s_fUi = 67;
        SVariables.s_fUj = 14;
        SVariables.s_fBaryI = SVariables.INVALID;
        SVariables.s_fBaryJ = SVariables.INVALID;
        SVariables.s_bUniformDistribution = true;
        SVariables.s_fRowPhotodiode = SVariables.INVALID;
        SVariables.s_fColPhotodiode = SVariables.INVALID;
        SVariables.s_fDelayPhotodiodeMs = 38;
        SVariables.s_fGraphicalStdX = 0.01f;
        SVariables.s_fGraphicalStdY = 0.1f;
    }

    [Test]
    public void Canary()
    {
        Assert.IsTrue(true);
    }

    [Test]
    public void RevertedAndNonRevertedScreens_RespReturn_XAndYLatencies()
    {
        /**
        * Since s_fUi is smaller than s_fUj, items are more grouped vertically than horizontally.
        * In other words, this means that the latency will be smaller vertically 
        * (non reverted screen) than horizontally (reverted screen).
        **/

        SVariables.s_fUi = 5;
        SVariables.s_fUj = 95;
        SVariables.s_fBaryI = 6;
        SVariables.s_fBaryJ = 6;

        float _0, _1;
        SVariables.s_eOrientation = SVariables.Orientation.REVERTED;
        float latency_reverted = SVariables.GetLatency(out _0, out _1);


        SVariables.s_eOrientation = SVariables.Orientation.NOT_REVERTED;
        float latency_nonreverted = SVariables.GetLatency(out _0, out _1);

        Assert.Greater(latency_reverted, latency_nonreverted);
    }

    [Test]
    public void UnknownOrientation_Returns_MaxOfRevertedAndNonRevertedLatencies()
    {
        /**
        * When the orientation of the screen is unknown, the latency is computed for the
        * two possible configurations (reverted or not-reverted).
        * The program shoud then returns the max of these latencies.
        **/

        SVariables.s_fUi = 5;
        SVariables.s_fUj = 95;

        float _0, _1;
        SVariables.s_eOrientation = SVariables.Orientation.REVERTED;
        float latency_reverted = SVariables.GetLatency(out _0, out _1);

        SVariables.s_eOrientation = SVariables.Orientation.NOT_REVERTED;
        float latency_nonreverted = SVariables.GetLatency(out _0, out _1);

        SVariables.s_eOrientation = SVariables.Orientation.UNKNOWN;
        float latency_unknown = SVariables.GetLatency(out _0, out _1);

        Assert.AreEqual(latency_unknown, Mathf.Max(latency_reverted, latency_nonreverted));
    }

    [Test]
    public void BarycenterWithLowNumberOfTargets_ShouldHave_HigherDeviationThanBarycenterWithHighNumberOfTargets()
    {
        /**
        * When there are only a few targets, 
        * their position can greatly vary on the screen.
        * At the inverse if there are as many targets than stimuli,
        * the position of the targets will match closely the position of the stimuli.
        * We expect the std deviation of the barycenter of these targets to reflect
        * this variability depending on the number of displayed targets.
        **/
    
        SVariables.s_iNumberOfTarget = 10;
        float std_low, _1;
        SVariables.GetLatency(out std_low, out _1);

        float std_high;
        SVariables.s_iNumberOfTarget = 100;
        SVariables.GetLatency(out std_high, out _1);

        Assert.Greater(std_low, std_high);
    }

    [Test]
    public void UniformDistributionWithHighNumberOfTargetBarycenter_ShouldBe_ScreenCenter()
    {
        /**
        * As said before, if there are at least as many targets
        * than there are stimuli, and that these targets are drawn
        * from an uniform distribution, then the position of these
        * targets will closely match the position of the stimuli.
        * Therefore, the barycenter of these targets should be the
        * center of the screen.
        **/
    
        SVariables.s_iNumberOfTarget = 5000;

        float screenCenterXAndY = 2.5f;

        //Center is expressed in number of targets
        Vector2 screenCenter = new Vector2(screenCenterXAndY, screenCenterXAndY);
        float _0, _1;
        // Barycenters are computed inside this methods
        SVariables.GetLatency(out _0, out _1);

        // Make sure to compute both X and Y coordinates of barycenter
        SVariables.s_eOrientation = SVariables.Orientation.NOT_REVERTED;
        SVariables.GetLatency(out _0, out _1);


        Vector2 barycenter = new Vector2(SVariables.s_fBaryI, SVariables.s_fBaryJ);
        float errorAcceptance = 0.001f;

        Assert.Greater(errorAcceptance, (screenCenter - barycenter).magnitude);
    }

    [Test]
    public void UnknownPhotodiodeWithUniformDistribution_ShouldHave_LocationSetAtMeanDistanceBtwnBarycenterAndBorder()
    {
        /**
        * When the position of the photodiode is not known (but we still have an estimation)
        * we set the position of this photodiode at the center between the position of the
        * barycenter and the farest border.
        * Because the standard deviation is set to the time required to refresh the screen
        * between the barycenter and the photodiode,
        * taking the farest versus the closest border allow to maximise the standard deviation.
        **/

        float errorAcceptance = 0.02f;

        float _0, _1;
        // Barycenters are computed inside this methods
        SVariables.GetLatency(out _0, out _1);

        float meanDistBtwnBaryAndBorder = 1.25f; //Assuming barycenter is located near to the centrum

        float distBtwnPhotodiodAndBary = Mathf.Abs(SVariables.s_fBaryI - SVariables.s_fColPhotodiode);
        Assert.Greater(errorAcceptance, Mathf.Abs(distBtwnPhotodiodAndBary - meanDistBtwnBaryAndBorder));

        // Test for reverted screen also
        SVariables.s_eOrientation = SVariables.Orientation.NOT_REVERTED;
        SVariables.GetLatency(out _0, out _1);
        distBtwnPhotodiodAndBary = Mathf.Abs(SVariables.s_fBaryJ - SVariables.s_fRowPhotodiode);
        Assert.Greater(errorAcceptance, Mathf.Abs(distBtwnPhotodiodAndBary - meanDistBtwnBaryAndBorder));
    }

    [Test]
    public void UnknownPhotodiodeWithUnknownDistribution_ShouldHave_LocationSetAtScreenCenter()
    {
        /**
        * When the position of the photodiode is not known (but we still have an estimation)
        * and the distribution of the targets is not known,
        * we set by design the position of the photodiode at the center of the screen.
        **/

        SVariables.s_bUniformDistribution = false;
        float screenCenter = 2.5f;

        float _0, _1;
        // Barycenters are computed inside this methods
        SVariables.GetLatency(out _0, out _1);
        Assert.AreEqual(SVariables.s_fColPhotodiode, screenCenter);

        // Test for reverted screen also
        SVariables.s_eOrientation = SVariables.Orientation.NOT_REVERTED;
        SVariables.GetLatency(out _0, out _1);
        Assert.AreEqual(SVariables.s_fRowPhotodiode, screenCenter);
    }

    [Test]
    public void ShouldReproduceCattan2018Results()
    {
        /**
        * Functional testing. Reproduce the analysis in 
        * Cattan et al. (2018).
        **/

        SVariables.s_fUj = 166f / 890f * 100f;
        SVariables.s_bUniformDistribution = true;
        SVariables.s_fRowPhotodiode = 2;
        SVariables.s_fColPhotodiode = 2;
        SVariables.s_fGraphicalStdX = 0.0f;
        SVariables.s_fGraphicalStdY = 0.0f;

        float std, error;
        float latency = SVariables.GetLatency(out std, out error);

        Assert.AreEqual(error, 32.03146f);
        Assert.Greater(1.6, latency);
        Assert.Greater(latency, 1.4);
        Assert.Greater(1.6, std);
        Assert.Greater(std, 1.4);
    }

    [Test]
    public void GetMWith2ItemsSpacedByHalfOfTheScreen_ShouldBe_25pOfTheScreen()
    {
        // [Unit testing]
        // M is the margin between a row/colum of stimuli and the border.
        // We assume it is the same at the beginning/end of the row/column.
        // Is there is two stimuli separated by half of the screen, then 
        // M must be 25% of the screen.
        // 
        // -------------------------
        // | <25%>  x <50%> x <M>  |
        // -------------------------
    
        float m = SCommon.GetM(100f, 2, 50);
        Assert.AreEqual(m, 25);
    }

    [Test]
    public void GetPhotodiodePlacedOnLastItemWith2ItemsSpacedByHalfOfTheScreen_ShouldBe_75pOfTheScreen()
    {
        // [Unit testing]
        // If two stimuli are speparated by half of the screen
        // And the photodiode is placed under the last stimulus
        // Then the position of the stimulus is at 75% of the screen.
        // 
        // -------------------------
        // | <25%>  x <50%> x      |
        // -------------------------
        // <---------75%---->
    
        float m = SCommon.GetPhotodiode(25, 50, 1);
        Assert.AreEqual(m, 75);
    }

    [Test]
    public void PScROfFirstItemWith2ItemsSpacedByHalfOfTheScreen_ShouldBe_HalfOfTheTimeForRefreshingTheScreen()
    {
        /**
        * [Unit testing]
        * If the screen takes 16ms to refresh, and two stimuli
        * are separated between half of the screen.
        * Then the second stimuli will be rendered 8ms after the first one.
        */
        float pscr = SCommon.PScR(1, 0.5f);
        Assert.AreEqual(pscr, SCommon.s_fRefreshScreenTime / 2);
    }

    [Test]
    public void PScRWithUInPercent_ShouldBe_TheSameThanWithNormalizedU()
    {
        /**
        * [Unit testing]
        * We can indicate the position of a stimulus 
        * using a percent of the screen (eg. 50%) or a fraction (0.5)
        */
        float pscr = SCommon.PScR(1, 0.5f);
        float pscrInPercent = SCommon.PScR(1, 50f);
        Assert.AreEqual(pscr, pscrInPercent);
    }

}