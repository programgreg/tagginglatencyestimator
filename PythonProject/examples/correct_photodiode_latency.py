from tle import TaggingComputer

"""
We want to estimate the tagging latency with this screen configuration:
- the screen is refreshing from left to right (reverted screen);
- the screen width is 1000 pxl;
- there is 3 items displayed in a row, and two in a column.
Since the screen is reverted, only the number of items in a row is meaningful.

Uj in the figure below is the distance between two items in a row
(that is 1000 / 4 = 250pxl).
If the screen were not reverted, you will have to consider Ui,
that is the distance between two items in a column.

```
          <--1000 pxl-->
----------------------------------
|       x <-Uj-> x        x      |
|       x        x        x      |
----------------------------------
```

Latency was first estimated using the method of the photodiode [1]_ to 40ms.
The photodiode was located on the second item on the first row (row=0,column=1)

Experiments includes the presentation of 10 stimuli, randomly selected based
on a uniform distribution.
A stimulus consists in flashing one of the potential targets.


References
----------
.. [1] A. Andreev, G. Cattan, and M. Congedo, \
    ‘Engineering study on the use of Head-Mounted display
    for Brain- Computer Interface’, \
    GIPSA-lab, Technical Report 1, juin 2019. \
    Available: https://hal.archives-ouvertes.fr/hal-02166844


"""


screen_width = 1000
items = 3
Uj = screen_width / (items + 1)
photodiode = 40
targets = 10

# Change this value to 0 or 2
col_photodiode = 1

tagging_computer = TaggingComputer(screenWidthInPixels=screen_width,
                                   itemNumberInARow=items,
                                   Uj=Uj,
                                   latencyMeasuredByPhotodiode=photodiode,
                                   nDisplayedTargets=targets,
                                   itemColCoordWherePhotodiode=col_photodiode)

tagging_computer.compute()

# A positive (negative) delay means that the barycenter
#  appears after (before) the stimulus observed by the photodiode
delay_btwn_bary_photo = tagging_computer.xLatency

# This only depends on the distribution of the targets on screen
std = tagging_computer.xLatencyStd

# The corrected latency is similar to the photodiode latency
# if the position of the photodiode matches the position of the barycenter
corrected_latency = photodiode + delay_btwn_bary_photo

print("ERP have a latency in the range: {mean:.3f} (+-{std:.3f})".
      format(mean=corrected_latency, std=std))
