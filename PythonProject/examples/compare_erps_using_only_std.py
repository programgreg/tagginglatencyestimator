from tle import TaggingComputer, common

"""
We want to compare the latency of two-event related potential under
two experimental conditions.

The configuration of the first experiment is:
- the screen is refreshing from left to right (reverted screen);
- the dimension of the screen is 1000 x 500 pxl;
- there is 3 items displayed in a row, and two in a column.
Since the screen is reverted, only the number of items in a row is meaningful.

Uj in the figure below is the distance between two items in a row
(that is 1000 / 4 = 250pxl).

```
          <--1000 pxl-->
------------------------------------
|                                  |
|       x  <-Uj-> x        x       |
|                                  |
|       x         x        x       |
|                                  |
------------------------------------
```

The configuration of the second experiment is exactly the same, except that
the screen refreshes form top to bottom (not reverted screen).
Since the screen is not reverted, only the number of items
in a column is meaningful.

Ui in the figure below is the distance between two items in a column
(that is 500 / 3 ~= 167pxl).

```
------------------------------------  ^
|                                  |
|    ^  x        x        x        |
|    Ui                            | 500
|    v  x        x        x        |
|                                  |
------------------------------------  v
```

No photodiode was used to estimate the average latency.
However, we can assume the latency is similar because the same plateform and
software are use between the two conditions.

Experiments includes the presentation of 2 stimuli,
drawn from a unifom distribution.
A stimulus consists in flashing one of the potential targets.

The refresh rate of the screen was set to 50Hz.


"""

# Let s the refresh rate of the screen. 50Hz corresponds to an image each 20s.
common.s_fRefreshScreenTime = 1000 / 50

# Let s enter the variables of our experiment for the first condition
screen_width = 1000
n_columns = 3
Uj = screen_width / (n_columns + 1)
targets = 2

tagging_computer = TaggingComputer(screenWidthInPixels=screen_width,
                                   itemNumberInARow=n_columns,
                                   Uj=Uj,
                                   nDisplayedTargets=targets)

# Let s enter the variables of our experiment for the second condition
screen_height = 500
n_rows = 2
Ui = screen_height / (n_rows + 1)
targets = 2

tagging_computer_2 = TaggingComputer(screenHeightInPixels=screen_height,
                                     itemNumberInACol=n_rows,
                                     Ui=Ui,
                                     nDisplayedTargets=targets)

# We compute the models
tagging_computer.compute()
tagging_computer_2.compute()

# Variabilities here solely depends on the distribution
# of our targets on screen and the configuration of the screen
std_bary = tagging_computer.xLatencyStd
std_bary_2 = tagging_computer_2.yLatencyStd

# Because the number of possible target in the second condition is lower,
# we should have `std_bary_2 < std_bary``

print("Std latency of the first and second condition resp.:\
      {std:.3f}, {std2:.3f}".
      format(std=std_bary, std2=std_bary_2))

# Let's compute the final std:

print("A delay of about:\
      {std:.3f} between the ERPs in the two conditions is acceptable.".
      format(std=std_bary + std_bary_2))
