from tle import get_m, get_photodiode, pscr, compute_barycenters


class TaggingComputer:
    """TaggingComputer

    Wrapper around `common.py`, to facilitate
    computation of tagging latency.

    Based on the photodiode method [1]_ to estimate latency.
    Correct the physical estimation and estimate incertainty [2, 3]_.

    Parameters
    ----------
    nTrialForBarycenterComputation : int (default: 10000)
        The number of iteration we will use to estimate the barycenters of the
        targets displayed on the screen. Estimation is more precise with
        higher value, but also takes more computational time.
    screenWidthInPixels : int (default: 890)
        Screen width in pixels.
    screenHeightInPixels : int (default: 500)
        Screen height in pixels.
    Ui : int (default: 87)
        Length (in pixels) between two consecutive items on a column.
    Uj : int (default: 166)
        Length (in pixels) between two consecutive items on a row.
    itemRowCoordWherePhotodiode : int (default: 2)
        Row coordinate of the stimulus used for estimating the latency thanks
        to the photodiode method [1]_.
    itemColCoordWherePhotodiode : int (default: 2)
        Column coordinate of the stimulus used for estimating the
        latency thanks to the photodiode method [1]_.
    nDisplayedTargets : int (default: 12)
        Number of display targets within the experiment.
    itemNumberInACol : int (default: 6)
        Number of items in a column.
    itemNumberInARow : int (default: 6)
        Number of items in a row.
    latencyMeasuredByPhotodiode : int (default: 38.0)
        Latency measure by the photodiode [1]_.

    Attributes
    ----------
    barycenterI : float
        Estimated row index of the barycenter.
    barycenterJ : float
        Estimated column index of the barycenter.
    barycenterStdI : float
        Standard deviation of the estimated row index
        for the barycenter.
    barycenterStdJ : float
        Standard deviation of the estimated column index
        for the barycenter.
    Mi : float
        Margin between the top (bottom) border and the first (last)
        row (in pixels).
    Mj : float
        Margin between the left (right) border and the first (last)
        column.
    photodiodeX : float
        Horizontal position of the photodiode in pixels.
    photodiodeY : float
        Vertical position of the photodiode in pixels.
    xLatency : float
        Horizontal latency between the photodiode and the
        estimated barycenter of all displayed targets.
    yLatency : float
        Vertical latency between the photodiode and the
        estimated barycenter of all displayed targets.
    xLatencyStd : float
        Standard deviation of the `xLatency` estimation.
    yLatencyStd : float
        Standard deviation of the `yLatency` estimation.
    platformLatencyX : float
        Estimation of the horizontal latency specific to the platform.
    platformLatencyY : float
        Estimation of the vertical latency specific to the platform.
    maxLatency : float
        If positive when maximum horizontal latency is greather than
        the maximum vertical latency. The maximum horizontal (vertical)
        latency is given by `xLatency + xLatencyStd` (yLatency + yLatencyStd).


    Notes
    -----
    .. versionadded:: 0.0.1

    See Also
    --------
    common.py

    References
    ----------
    .. [1] A. Andreev, G. Cattan, and M. Congedo, \
           ‘Engineering study on the use of Head-Mounted display for
           Brain- Computer Interface’, \
           GIPSA-lab, Technical Report 1, juin 2019. \
           Available: https://hal.archives-ouvertes.fr/hal-02166844

    .. [2] G. Cattan, A. Andreev, B. Maureille, and M. Congedo, \
           ‘Analysis of tagging latency when comparing
            event-related potentials’, \
            Gipsa-Lab ; IHMTEK, Grenoble, Technical Report 1, décembre 2018. \
            Available: https://hal.archives-ouvertes.fr/hal-01947551

    .. [3] G. Cattan, ‘De la réalisation d’une interface cerveau-ordinateur
           pour une réalité virtuelle accessible au grand public’,
           Thesis, Université Grenoble Alpes, 2019. \
           Available: https://tel.archives-ouvertes.fr/tel-02357203


    """

    def __init__(self, nTrialForBarycenterComputation=10000,
                 screenWidthInPixels=890,
                 screenHeightInPixels=500,
                 Ui=87,
                 Uj=166,
                 itemRowCoordWherePhotodiode=2,
                 itemColCoordWherePhotodiode=2,
                 nDisplayedTargets=12,
                 itemNumberInACol=6,
                 itemNumberInARow=6,
                 latencyMeasuredByPhotodiode=38.0):
        self.nTrialForBarycenterComputation = nTrialForBarycenterComputation
        self.screenWidthInPixels = screenWidthInPixels
        self.screenHeightInPixels = screenHeightInPixels
        self.Ui = Ui
        self.Uj = Uj
        self.itemRowCoordWherePhotodiode = \
            itemRowCoordWherePhotodiode
        self.itemColCoordWherePhotodiode = \
            itemColCoordWherePhotodiode
        self.nDisplayedTargets = nDisplayedTargets
        self.itemNumberInACol = itemNumberInACol
        self.itemNumberInARow = itemNumberInARow
        self.latencyMeasuredByPhotodiode = latencyMeasuredByPhotodiode
        # to compute
        self.barycenterI = None
        self.barycenterJ = None
        self.barycenterStdI = None
        self.barycenterStdJ = None
        self.Mi = None
        self.Mj = None
        self.photodiodeX = None
        self.photodiodeY = None
        self.xLatency = None
        self.yLatency = None
        self.xLatencyStd = None
        self.yLatencyStd = None
        self.platformLatencyX = None
        self.platformLatencyY = None
        self.maxLatency = None
        self.compute()

    def _set_m(self):
        self.Mi = get_m(self.screenHeightInPixels, self.itemNumberInACol,
                        self.Ui)
        self.Mj = get_m(self.screenWidthInPixels, self.itemNumberInARow,
                        self.Uj)

    def _get_photodiode_y(self, a_I):
        return get_photodiode(self.Mi, self.Ui, a_I) / \
            self.screenHeightInPixels

    def _get_photodiode_x(self, a_J):
        return get_photodiode(self.Mj, self.Uj, a_J) / \
            self.screenHeightInPixels

    def _compute_barycenters(self):
        self.barycenterI,\
            self.barycenterJ,\
            self.barycenterStdI,\
            self.barycenterStdJ = \
            compute_barycenters(self.nTrialForBarycenterComputation,
                                self.nDisplayedTargets,
                                self.itemNumberInACol,
                                self.itemNumberInARow)

    def _pscr_x(self, h):
        UjNormalized = self.Uj / self.screenWidthInPixels
        return pscr(h, UjNormalized)

    def _pscr_y(self, h):
        UiNormalized = self.Ui / self.screenHeightInPixels
        return pscr(h, UiNormalized)

    def compute(self):
        """Estimate the barycenters and the latency to the photodiode.
        Estimate the platform latency. Compute a confidence interval (std).
        """
        self._set_m()
        self._compute_barycenters()
        self.photodiodeX = \
            self._get_photodiode_x(self.itemColCoordWherePhotodiode)
        self.photodiodeY = \
            self._get_photodiode_y(self.itemRowCoordWherePhotodiode)
        self.xLatency = \
            self._pscr_x(self.barycenterJ - self.itemColCoordWherePhotodiode)
        self.yLatency = \
            self._pscr_y(self.barycenterI - self.itemRowCoordWherePhotodiode)
        self.xLatencyStd = self._pscr_x(self.barycenterStdJ)
        self.yLatencyStd = self._pscr_y(self.barycenterStdI)
        self.platformLatencyX = self.latencyMeasuredByPhotodiode - \
            self._pscr_x(self.itemColCoordWherePhotodiode)
        self.platformLatencyY = self.latencyMeasuredByPhotodiode - \
            self._pscr_y(self.itemRowCoordWherePhotodiode)
        self.maxLatency = self.xLatency + self.xLatencyStd - \
            self.yLatency - self.yLatencyStd
