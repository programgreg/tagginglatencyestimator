from math import sqrt
from random import Random


s_fRefreshScreenTime = 16


def pscr(a_fHeight, a_fU):
    """Perceived Screen Rendering

        Parameters
        ----------
        a_fHeight : float
            A portion of the screen, expressed in number of items.
            Example:
            Let' s say you have 2 items on the screen spaced
            by half of the screen and you want to know the time
            needed to refresh the first item then you should set
            a_fHeight to 1 and a_fU to 50 or 0.5.
        a_fU : float
            Uj or Ui, that is the distance between two consecutive
            elements in a row or a column.
            Must be espressed a percent of the screen
            (in the range 0..1 or 0..100)

        Returns
        -------
        pscr : float
            Perceived amount of time for a screen to refresh
            a certain portion of the screen.
    """
    # Normalize Uj or Ui between 0 and 1 if given as percent
    if a_fU > 1:
        a_fU /= 100
    return s_fRefreshScreenTime * a_fU * a_fHeight


def get_m(a_fScreenDim, a_iNumItems, a_fU):
    """Mi or Mj

        Parameters
        ----------
        a_fScreenDim : float
            Width or height of the screen (in pixels)
        a_iNumItems : float
            Number of items in a row or a column
        a_fU : float
            Uj or Ui, that is the distance between two consecutive
            elements in a row or a column.
            Must be espressed a percent of the screen
            (in the range 0..1 or 0..100)

        Returns
        -------
        m : float
            Margin between a vertical (horizontal) border of the screen
            and a row (column)
    """
    return (a_fScreenDim - (a_iNumItems - 1) * a_fU) / 2


def get_photodiode(a_fM, a_fU, a_fCoordPhotodiode):
    """Mi or Mj

        Parameters
        ----------
        a_fM : float
            Mi or Mj
        a_fU : float
            Uj or Ui, that is the distance between two consecutive
            elements in a row or a column.
            Must be espressed a percent of the screen
            (in the range 0..1 or 0..100)
        a_fCoordPhotodiode : float
            Column or row index of the item that the photodiode
            was observing.
            Item index starts at 0

        See Also
        --------
        get_m

        Returns
        -------
        coord_photodiode : float
            the coordinate of the photodiode in screen percent.
    """
    return a_fM + a_fU * a_fCoordPhotodiode


def compute_barycenters_std(a_iNumTrials, a_afBarycentersI, a_afBarycentersJ,
                            a_fBarycenterI, a_fBarycenterJ):
    """Estimate standard deviation in the computation of barycenters.

        Parameters
        ----------
        a_iNumTrials : int
            Number of trials used to compute barycenters.
        a_afBarycentersI : list[float]
            Row indices of generated barycenters in the matrix of items.
        a_afBarycentersJ : list[float]
            Column indices of generated barycenters in the matrix of items.
        a_fBarycenterI : float
            Mean row index of generated barycenters in the matrix of items.
        a_fBarycenterJ : float
            Mean column index of generated barycenters in the matrix of items.

        Returns
        -------
        fBarycenterStdI : float
            Standard deviation of the barycenters in row index.
        fBarycenterStdJ : float
            Standard deviation of the barycenters in column index.
    """
    fBarycenterStdI = 0.0
    fBarycenterStdJ = 0.0
    for i in range(a_iNumTrials):
        fBarycenterStdI += (a_afBarycentersI[i] - a_fBarycenterI) ** 2
        fBarycenterStdJ += (a_afBarycentersJ[i] - a_fBarycenterJ) ** 2

    fBarycenterStdI = sqrt(fBarycenterStdI / a_iNumTrials)
    fBarycenterStdJ = sqrt(fBarycenterStdJ / a_iNumTrials)
    return fBarycenterStdI, fBarycenterStdJ


def compute_barycenter(a_oRandomGenerator, a_iNumTargets,
                       a_iNumRow, a_iNumCol):
    """Choose (uniform distribution) `a_iNumTargets` in the matrix
        `a_iNumRow` x `a_iNumbCol`and compute their barycenter.

        Parameters
        ----------
        a_oRandomGenerator : Random
            An instance of Random generator.
        a_iNumTargets : int
            Number of random targets to generate.
        a_iNumRow : int
            Number of items in a row.
        a_iNumCol : int
            Number of items in a column.

        Returns
        -------
        fBaryI : float
            Row index of the barycenter.
        fBaryJ : float
            Column index of the barycenter.
    """
    fBaryI = 0.0
    fBaryJ = 0.0
    for i in range(a_iNumTargets):
        fBaryI += a_oRandomGenerator.randint(0, a_iNumRow - 1)
        fBaryJ += a_oRandomGenerator.randint(0, a_iNumCol - 1)
    fBaryI /= a_iNumTargets
    fBaryJ /= a_iNumTargets
    return fBaryI, fBaryJ


def compute_barycenters(a_iNumTrials, a_iNumTargets, a_iNumRow, a_iNumCol):
    """Choose (uniform distribution) `a_iNumTargets` in the matrix
        `a_iNumRow` x `a_iNumbCol`and compute their barycenter.

        Parameters
        ----------
        a_iNumTrials : int
            Number of trials used to compute barycenters.
        a_iNumTargets : int
            Number of random targets to generate by trial.
        a_iNumRow : int
            Number of items in a row.
        a_iNumCol : int
            Number of items in a column.

        Returns
        -------
        fBaryI : float
            Mean row index of the barycenter.
        fBaryJ : float
            Mean column index of the barycenter.
        fMeanStdI : float
            Standard deviation of the barycenters in row index.
        fMeanStdJ : float
            Standard deviation of the barycenters in column index.
    """
    oRandomGenerator = Random()
    afBarycentersI = [0.0] * a_iNumTrials
    afBarycentersJ = [0.0] * a_iNumTrials
    fBaryI = 0.0
    fBaryJ = 0.0
    for i in range(a_iNumTrials):
        ftmpBaryI, ftmpBaryJ = \
            compute_barycenter(oRandomGenerator, a_iNumTargets,
                               a_iNumRow, a_iNumCol)
        afBarycentersI[i] = ftmpBaryI
        afBarycentersJ[i] = ftmpBaryJ
        fBaryI += ftmpBaryI
        fBaryJ += ftmpBaryJ
    fBaryI /= a_iNumTrials
    fBaryJ /= a_iNumTrials
    fMeanStdI, fMeanStdJ = compute_barycenters_std(a_iNumTrials,
                                                   afBarycentersI,
                                                   afBarycentersJ,
                                                   fBaryI,
                                                   fBaryJ)
    return fBaryI, fBaryJ, fMeanStdI, fMeanStdJ
