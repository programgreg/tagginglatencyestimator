from tle import get_m, get_photodiode, pscr, s_fRefreshScreenTime


def test_canary():
    assert True


def test_get_m():
    # See TestSuite.cs/
    #   GetMWith2ItemsSpacedByHalfOfTheScreen_ShouldBe_25pOfTheScreen
    m = get_m(100, 2, 50)
    assert m == 25


def test_get_photodiode():
    # See TestSuite.cs/
    #   GetPhotodiodePlacedOnLastItemWith2ItemsSpacedByHalfOfTheScreen_ShouldBe_75pOfTheScreen
    m = get_photodiode(25, 50, 1)
    assert m == 75


def test_pscr_parameter_contract():
    # See TestSuite.cs
    #   PScRWithUInPercent_ShouldBe_TheSameThanWithNormalizedU
    pscrDecimal = pscr(1, 0.5)
    pscrInPercent = pscr(1, 50)
    assert pscrDecimal == pscrInPercent


def test_pscr():
    # See TestSuite.cs/
    #   PScROfFirstItemWith2ItemsSpacedByHalfOfTheScreen_ShouldBe_HalfOfTheTimeForRefreshingTheScreen
    value = pscr(1, 0.5)
    assert value == s_fRefreshScreenTime / 2
