from pytest import approx
from tle import TaggingComputer


def test_canary():
    tc = TaggingComputer()
    tc.compute()


def test_default_results():
    # See TestSuite.cs/
    #   ShouldReproduceCattan2018Results
    tc = TaggingComputer()
    tc.compute()
    assert tc.barycenterI == approx(2.5, 0.01)
    assert tc.barycenterJ == approx(tc.barycenterI, 0.01)
    assert tc.barycenterStdI == approx(0.49, 0.1)
    assert tc.barycenterStdJ == approx(tc.barycenterStdI, 0.1)
    assert tc.Mi == approx(32.5)
    assert tc.Mj == approx(30)
    assert tc.photodiodeX == approx(0.72, 0.01)
    assert tc.photodiodeY == approx(0.41, 0.01)
    assert tc.xLatency == approx(1.48, 0.1)
    assert tc.yLatency == approx(1.40, 0.1)
    assert tc.xLatencyStd == approx(1.47, 0.1)
    assert tc.yLatencyStd == approx(1.37, 0.1)
    assert tc.platformLatencyX == approx(32.03, 0.01)
    assert tc.platformLatencyY == approx(32.43, 0.01)
    assert tc.maxLatency == approx(0.17, 1)
