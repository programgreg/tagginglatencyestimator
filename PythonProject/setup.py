import os.path as op

from setuptools import setup, find_packages


# get the version (don't import mne here, so dependencies are not needed)
version = None
with open(op.join('tle', '_version.py'), 'r') as fid:
    for line in (line.strip() for line in fid):
        if line.startswith('__version__'):
            version = line.split('=')[1].strip().strip('\'')
            break
if version is None:
    raise RuntimeError('Could not determine version')

with open('../README.md', 'r', encoding="utf8") as fid:
    long_description = fid.read()

setup(name='tle',
      version=version,
      description='Tagging Latency Estimator',
      url='https://gitlab.com/programgreg/tagginglatencyestimator',
      author='Gregoire Cattan',
      author_email='gcattan@hotmail.com',
      license='BSD (3-clause)',
      packages=find_packages(),
      long_description=long_description,
      long_description_content_type='text/markdown',
      project_urls={
          'Documentation': 'https://gitlab.com/programgreg/tagginglatencyestimator',
          'Source': 'https://gitlab.com/programgreg/tagginglatencyestimator',
          'Tracker': 'https://gitlab.com/programgreg/tagginglatencyestimator/-/issues',
      },
      platforms='any',
      python_requires=">=3.6",
      install_requires=[],
      extras_require={'tests': ['pytest', 'flake8']},
      zip_safe=False,
    )
